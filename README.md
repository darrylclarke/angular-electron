# Angular Test Repo
This repo has a small AngularJS app written three ways.  You can look at the individual branches
to see the differences in each style.

## ModernAdvisor `directive` style

> branch `implement-as-directives`
 
Most of ModernAdvisor's code uses directives.  Older Angular versions didn't support
components and we used directives to approximate a modular component-based style.

Have a look at this branch to see something similar to what we do in most of our code.

## Newer `component` style

> branch `master`

Later AngularJS versions support components.  Components have fewer knobs you can turn, 
so they are simpler to write and understand.  They also support one-way data binding,
which is cleaner and recommended by the Angular team.

I personally like the component style because there is less indentation and nesting, and
you can use Constructor functions for the component constructors.

It also lends itself very nicely to programming in an Angular 2 style.  See the next section.

## Newer `component` in ES6 with Angular 2 style

> branch `es6-class-test`

This is the same as the above, but one of the components has been ported to ES6 and 
re-factored in an Angular 2 style.

