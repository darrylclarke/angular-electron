const angular = require('angular');

angular.module('heroApp')
  .component('basicFileList', {
    template:
      `
<div ng-repeat="item in $ctrl.items">
<span ng-class="{'done': item.done}"  
      ng-click="$ctrl.clicked(item.html)">{ {{item.page}}:  {{item.html}} }</span>
</div>
`,
    bindings: {
      items: '<',
      onClick: '&',
    },
    controllerAs: '$ctrl',
    controller: function() {
      const $ctrl = this;

      $ctrl.clicked = function(fileName) {
        $ctrl.onClick({fileName})
      }
    }
  });

