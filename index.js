const {ipcRenderer} = require('electron');
const angular = require('angular');

angular.module('helperApp', [])
  .controller('MainCtrl', function MainCtrl($scope) {
    const $ctrl = this;

    ipcRenderer.send('get-data');

    $ctrl.onClick = function(fileName) {
      console.log(fileName);
      ipcRenderer.send('get-file', {fileName: fileName});
    };

    ipcRenderer.on('data-callback', (event, props) => {
      console.log(props.data);
      $ctrl.items = props.data;
      $scope.$digest();
    });

    ipcRenderer.on('file-callback', (event, props) => {
      if (props.error) {
        alert(props.error);
      } else {
        console.log(`Text copied for ${props.fileName}`);
        $ctrl.items = $ctrl.items.map(item => {
          return item.fileName === props.fileName ? Object.assign({}, item, {done: true}) : item;
        });
        $scope.$apply();
      }
    });
  });
