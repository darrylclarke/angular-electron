const {app, ipcMain, BrowserWindow, clipboard} = require('electron');
const path = require('path');
const fs = require('fs');
const yaml = require('js-yaml');

let mainWindow;
const HTML_BASE = '/Users/darryl/prj/modernadvisor-marketing/code/html';

const debug = process.argv[2] === 'debug';
const options = debug ? {
  height: 800,
  width: 1200
} : {
  height: 500,
  width: 400
};

app.on('ready', () => {
  mainWindow = new BrowserWindow(options);
  mainWindow.loadURL(path.join('file://', __dirname, 'index.html'));
  debug && mainWindow.webContents.openDevTools();
});

ipcMain.on('get-data', () => {
  loadYaml('./data.yml')
    .then((dataLines) => mainWindow.webContents.send('data-callback', {data: dataLines.map(line => Object.assign({}, line, {done: false}))}))
    .catch(() => alert(`Can't get data!`));
});

ipcMain.on('get-file', (event, props) => {
  const fileName = HTML_BASE + '/' + props.fileName;
  fs.readFile(fileName, 'utf8', function(e, data) {
    if (e) {
      mainWindow.webContents.send('file-callback', {error: 'File not found!'})
    } else {
      clipboard.writeText(data + "\n");
      mainWindow.webContents.send('file-callback', {success: true, fileName: props.fileName})
    }
  });
});

function loadYaml(fileName) {
  return new Promise((resolve, reject) => {
    fs.readFile(fileName, 'utf8', function(e, data) {
      if (e) {
        reject(`${fileName} not found.`);
      } else {
        const file = yaml.safeLoad(data, 'utf8');
        resolve(file);
      }
    });
  });
}
