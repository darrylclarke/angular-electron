const angular = require('angular');

angular.module('heroApp')
  .component('heroBook', {
    templateUrl: './hero-book/heroBook.html',
    bindings: {},
    controllerAs: '$ctrl',
    controller: HeroBookController
  });

function HeroBookController() {
}

HeroBookController.prototype.$onInit = function() {
  this.hero = {
    power: 'Spawn'
  };

  this.hasPower = true;
};

HeroBookController.prototype.activate = function(name, power, powerInEffect) {
  if (powerInEffect === true || powerInEffect === false) {
    this.hasPower = powerInEffect;
    return;
  }

  if (power && name) {
    alert(`${name}'s power is ${power}.`);
  } else if (power) {
    alert(`Your un-named hero has power ${power}`);
  } else if (name) {
    alert(`${name} has no power.`);
  } else {
    alert(`That's a boring superhero movie.`);
  }
};
