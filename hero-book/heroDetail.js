const angular = require('angular');

angular.module('heroApp')
  .component('heroDetail', {
    templateUrl: './hero-book/heroDetail.html',
    bindings: {
      heroName: '<',
      heroDetails: '<',
      powerInEffect: '<',
      onActivate: '&',
    },
    controllerAs: '$ctrl',
    controller: HeroDetailController
  });

function HeroDetailController() {
}

HeroDetailController.prototype.done = function() {
  this.onActivate({name: this.heroName, power: this.powerInEffect && this.heroDetails.power});
};
