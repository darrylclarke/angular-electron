const angular = require('angular')

angular.module('helperApp')
  .component('app', {
    templateUrl: './helper/app.html',
    bindings: {
      data: '<',
      onClick: '&'
    },
    controllerAs: "$ctrl",
    controller: AppController
  });

function AppController() {}

AppController.prototype.copy = function (fileName) {
  this.onClick({fileName: fileName});
  alert(`Copied ${fileName} to clipboard.`);
};
