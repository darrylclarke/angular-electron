const angular = require('angular')

angular.module('helperApp')
  .component('pageData', {
    templateUrl: './helper/pageData.html',
    bindings: {
      display: '<',
      fileName: '<',
      done: '<',
      copyAction: '&',
    },
    controllerAs: "$ctrl",
    controller: PageDataController
  });

function PageDataController() {

}
